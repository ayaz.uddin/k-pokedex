"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const tsoa_1 = require("tsoa");
let AuthController = class AuthController extends tsoa_1.Controller {
    login(body) {
        const { email, password } = body;
        // check against db if email and pw match
        // if so login!
        // add prisma
    }
    logout() {
        return;
    }
    signUp(body) {
        const { email, password } = body;
        return;
    }
    refreshTokens(body) {
        return;
    }
};
__decorate([
    (0, tsoa_1.Post)('/login'),
    __param(0, (0, tsoa_1.Body)())
], AuthController.prototype, "login", null);
__decorate([
    (0, tsoa_1.Post)('/logout')
], AuthController.prototype, "logout", null);
__decorate([
    (0, tsoa_1.Post)('/sign-up'),
    __param(0, (0, tsoa_1.Body)())
], AuthController.prototype, "signUp", null);
__decorate([
    (0, tsoa_1.Post)('/refresh'),
    __param(0, (0, tsoa_1.Header)())
], AuthController.prototype, "refreshTokens", null);
AuthController = __decorate([
    (0, tsoa_1.Route)('/auth')
], AuthController);
exports.AuthController = AuthController;
