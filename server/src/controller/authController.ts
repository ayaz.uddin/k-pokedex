import { Post, Controller, Route, Body, Header } from 'tsoa'

type AuthParams = {
    email: string,
    password: string
}

type HeaderParams = {
    accessToken: string,
    refreshToken: string
}

@Route('/auth')
export class AuthController extends Controller {

    @Post('/login')
    login(
        @Body() body: AuthParams
    ) {
        const { email, password } = body;
        // check against db if email and pw match
        // if so login!

        // add prisma
    }

    @Post('/logout')
    logout() {
        return
    }

    @Post('/sign-up')
    signUp(
        @Body() body: AuthParams
    ) {
        const { email, password } = body;
        return
    }

    @Post('/refresh')
    refreshTokens(
        @Header() header: HeaderParams
    ) {
        const { accessToken, refreshToken } = header;
        return
    }
}