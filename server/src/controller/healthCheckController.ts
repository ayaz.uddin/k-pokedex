import { Get, Controller, Route } from 'tsoa'

@Route('/health')
export class HealthController extends Controller {
  @Get()
  isHealthy(): { message: string } {
    return  { message: 'health!' }
  }
}